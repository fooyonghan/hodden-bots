import { render } from 'react-dom'
import Router from './router/routes'
import store from './store/store'

const run = () => {
	render(Router(store), document.getElementById('root'))
}

if(['complete', 'loaded', 'interactive'].includes(document.readyState) && document.body) {
	run()
} else {
	document.addEventListener('DOMContentLoaded', run, false)
}
