// Action Types
export const SHOW_MORE_INFO_CARD = 'SHOW_MORE_INFO_CARD'
export const HIDE_MORE_INFO_CARD = 'HIDE_MORE_INFO_CARD'

// Action Creators
export function showMoreInfoCard(card) {
	return {
		type: SHOW_MORE_INFO_CARD,
		card
	}
}

export function hideMoreInfoCard() {
	return {
		type: HIDE_MORE_INFO_CARD,
	}
}
