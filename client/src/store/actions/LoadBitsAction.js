// Action Types
export const ITEM_HAS_ERRORED = 'ITEM_HAS_ERRORED'
export const ITEM_IS_LOADING = 'ITEM_IS_LOADING'
export const ITEM_FETCH_DATA_SUCCESS = 'ITEM_FETCH_DATA_SUCCESS'

// Action Creators
export function itemsHasErrored(bool) {
	return {
		type: ITEM_HAS_ERRORED,
		hasErrored: bool
	}
}

export function itemsIsLoading(bool) {
	return {
		type: ITEM_IS_LOADING,
		isLoading: bool
	}
}

export function itemsFetchDataSuccess(items) {
	return {
		type: ITEM_FETCH_DATA_SUCCESS,
		items
	}
}

export function itemsFetchData(url) {
	return (dispatch) => {
		dispatch(itemsIsLoading(true))

		fetch(url)
			.then((response) => {
				if (!response.ok) {
					throw Error(response.statusText)
				}

				dispatch(itemsIsLoading(false))

				return response
		})
			.then((response) => response.json())
			.then((items) => dispatch(itemsFetchDataSuccess(items)))
			.catch(() => dispatch(itemsHasErrored(true)))
	}
}