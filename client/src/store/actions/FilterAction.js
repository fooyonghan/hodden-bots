// Action Types
export const SHOW_TINY_BITS = 'SHOW_TINY_BITS'
export const SHOW_MASSIVE_BITS = 'SHOW_MASSIVE_BITS'
export const SHOW_HIDDEN_BITS = 'SHOW_HIDDEN_BITS'

// Action Creators
export function showTinyBits() {
	return {
		type: SHOW_TINY_BITS
	}
}

export function showMassiveBits() {
	return {
		type: SHOW_MASSIVE_BITS
	}
}

export function showHiddenBits() {
	return {
		type: SHOW_HIDDEN_BITS
	}
}

