// Action Types
export const SHOW_NAVIGATION_MENU = 'SHOW_NAVIGATION_MENU'
export const HIDE_NAVIGATION_MENU = 'HIDE_NAVIGATION_MENU'

// Action Creators
export function showMobileNavigationMenu() {
	return {
		type: SHOW_NAVIGATION_MENU,
	}
}

export function hideMobileNavigationMenu() {
	return {
		type: HIDE_NAVIGATION_MENU,
	}
}
