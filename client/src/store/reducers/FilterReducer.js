import {
	SHOW_TINY_BITS,
	SHOW_MASSIVE_BITS,
	SHOW_HIDDEN_BITS
} from '../actions/FilterAction'

const DEFAULT_STATE = {
	filterType: 'hiddenbits'
}

export function toggleFilter( state = DEFAULT_STATE, action ) {
	switch ( action.type ) {
		case SHOW_HIDDEN_BITS:
			return { ...state, filterType: 'hiddenbits'}
		case SHOW_TINY_BITS:
			return { ...state, filterType: 'tinybits' }
		case SHOW_MASSIVE_BITS:
			return { ...state, filterType: 'massivebits' }
		default:
			return state
	}
}

export default toggleFilter
