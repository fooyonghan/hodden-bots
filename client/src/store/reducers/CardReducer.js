import {
	SHOW_MORE_INFO_CARD,
	HIDE_MORE_INFO_CARD
} from '../actions/CardAction'

const DEFAULT_STATE = {
	visibleMoreInfoCard: false,
	card: {}
}

export function toggleMoreInfoCard( state = DEFAULT_STATE, action ) {
	switch ( action.type ) {
		case SHOW_MORE_INFO_CARD:
			return { ...state, visibleMoreInfoCard: true, card: action.card }
		case HIDE_MORE_INFO_CARD:
			return { ...state, visibleMoreInfoCard: false }
		default:
			return state
	}
}

export default toggleMoreInfoCard