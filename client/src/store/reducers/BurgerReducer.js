import {
	SHOW_NAVIGATION_MENU,
	HIDE_NAVIGATION_MENU,
} from '../actions/EyeAction'

const DEFAULT_STATE = {
	visibleMobileMenu: false
}

function toggleNavigationMenu( state = DEFAULT_STATE, action ) {
	switch ( action.type ) {
		case SHOW_NAVIGATION_MENU:
			return { ...state, visibleMobileMenu: true }
		case HIDE_NAVIGATION_MENU:
			return { ...state, visibleMobileMenu: false }
		default:
			return state
	}
}

export default toggleNavigationMenu
