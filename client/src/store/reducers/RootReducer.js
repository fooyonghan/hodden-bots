import { combineReducers } from 'redux'
import BurgerReducer from './BurgerReducer'
import { itemsHasErrored, itemsIsLoading, items } from './LoadBitsReducer'
import CardReducer from './CardReducer'
import FilterReducer from './FilterReducer'

export default combineReducers({
	BurgerReducer,
	itemsHasErrored, itemsIsLoading, items,
	CardReducer,
	FilterReducer
})
