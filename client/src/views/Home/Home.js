import React, { Component } from 'react'
import BitsOverviewContainer from '../../containers/BitsOverviewContainer'

class Home extends Component {
	render () {
		return (
			<div className="Home container">
				<BitsOverviewContainer></BitsOverviewContainer>
			</div>
		)
	}
}

export default Home
