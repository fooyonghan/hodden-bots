import React, { Component } from 'react'
import MoreInfoCardContainer from '../../containers/MoreInfoCardContainer'
import BackgroundAnimation from '../../components/ContactViewComponents/BackgroundAnimation/BackgroundAnimation'

import './Contact.scss'

class Contact extends Component {
	render () {
		return (
			<div className="contact">
				<div className="business-card">
					<h3>Lie Tiong Han</h3>
					<h4>tionghan.lie@gmail.com</h4>
				</div>
				<BackgroundAnimation />
				<MoreInfoCardContainer />
			</div>
		)
	}
}

export default Contact
