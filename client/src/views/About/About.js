import React, { Component } from 'react'
import MoreInfoCardContainer from '../../containers/MoreInfoCardContainer'
import Text from '../../components/AboutViewComponents/Text/Text'
import GridShape from '../../components/AboutViewComponents/Shapes/GridShape/GridShape'

import AOS from 'aos'
import 'aos/dist/aos.css'

import './About.scss'

class About extends Component {
	constructor(props) {
		super(props)

		this.state = {
			fullText: [
				{ 
					id: 1,
					text: '&lt;strong&gt;Hidden Bits&lt;/strong&gt; is a &lt;em&gt; tiny museum &lt;/em&gt; of &lt;em&gt; little bits &lt;/em&gt; I made.',
					shape: 'eye'
				},
				{
					id: 2,
					text: 'Some were for work or university, others for friends or fun.',
					shape: 'zen'
				},
				{
					id: 3,
					text: 'It is a place where I can collect and display the things I made that were &lt;em&gt;interesting&lt;/em&gt;, &lt;em&gt;dumb&lt;/em&gt; or &lt;em&gt;beautiful&lt;/em&gt;.',
					shape: 'monstera'
				},
				{
					id: 4,
					text: 'Thank you for passing by and have a great day!',
					shape: 'keith'
				},
				{
					id: 5,
					text: 'Love,&lt;br/&lt;&lt;em&gt;Han&lt;/em&gt;',
					shape: 'heart'
				}
			]
		}
	}

	componentDidMount(){
		AOS.init({
			duration : 2000
		})
	}
	render () {
		return (
			<div className="about">
				<ul>
					{this.state.fullText.map((infoBit) =>
					(
						<li key = { infoBit.id }>
							<section>
								<Text text = { infoBit.text } />
								<GridShape shape = { infoBit.shape }/>
							</section>
						</li>
					))}
				</ul>
				<MoreInfoCardContainer/>
			</div>
		)
	}
}

export default About
