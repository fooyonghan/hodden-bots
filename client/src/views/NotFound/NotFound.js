import React, { Component } from 'react'

class NotFound extends Component {
	render () {
		return (
			<div className="NotFound">
				<h1>
					Oopsie! This page does not exist. Existential crisis alert!
				</h1>
			</div>
		)
	}
}

export default NotFound
