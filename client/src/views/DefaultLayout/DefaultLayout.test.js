import React from 'react'
import DefaultLayout from './DefaultLayout'
import { shallow } from '../../tests/setupTests'

describe('Default layout view', () => {
	it('renders without crashing', () => {
		const component = shallow(<DefaultLayout />)
		expect(component).toMatchSnapshot()
	})
})
