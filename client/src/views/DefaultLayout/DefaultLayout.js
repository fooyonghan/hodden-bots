import React from 'react'
import { Route } from 'react-router-dom'

import HeaderContainer from '../../containers/HeaderContainer'
import Footer from '../../components/GeneralComponents/Footer/Footer'

import './DefaultLayout.scss'

const DefaultLayout = ({component: Component, ...rest}) => {
	return (
		<Route {...rest} render = { matchProps => (
			<div className="defaultLayout">
				<HeaderContainer />
				<Component {...matchProps} />
				<Footer className="container" />
			</div>
		)} />
	)
}

export default DefaultLayout
