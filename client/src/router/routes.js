import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'

import DefaultLayout from '../views/DefaultLayout/DefaultLayout'
import Home from '../views/Home/Home'
import About from '../views/About/About'
import Contact from '../views/Contact/Contact'
import NotFound from '../views/NotFound/NotFound'

export default (store) => {
	return (
		<Provider store={store}>
			<BrowserRouter>
				<Switch>
					<DefaultLayout exact path="/" component = { Home }/>
					<DefaultLayout exact path="/about" component = { About }/>
					<DefaultLayout exact path="/contact" component = { Contact }/>
					<Route path="*" component = { NotFound }/>
				</Switch>
			</BrowserRouter>
		</Provider>
	)
}
