import React from 'react'
import BitArtwork from '../BitArtwork/BitArtwork'
import CardContainer from '../../../containers/CardContainer'

import './Bit.scss'

const Bit = ({
	id,
	homie,
	dateofbirth,
	title,
	datecreated,
	description,
	projectlink,
	toolsused
}) =>
	<div className="bit fade-in-bottom"
		id = { id }
		homie = { homie }
		dateofbirth = { dateofbirth }
		title = { title }
		datecreated = { datecreated }
		description = { description }
		projectlink = { projectlink }
		toolsused = { toolsused }
	>
		<BitArtwork id={id}></BitArtwork>
		<CardContainer
			homie = { homie }
			dateofbirth = { dateofbirth }
			title = { title }
			datecreated = { datecreated }
			description = { description }
			projectlink = { projectlink }
			toolsused = { toolsused }
		></CardContainer>
	</div>

export default Bit