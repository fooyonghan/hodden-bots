import React, { Component } from 'react'

import Bit from '../Bit/Bit'
import MoreInfoCardContainer from '../../../containers/MoreInfoCardContainer'
import './BitsOverview.scss'

class BitsOverview extends Component {
	componentDidMount() {
		this.props.fetchData('https://res.cloudinary.com/fooyonghan/image/list/hiddenbits.json')
	}

	render() {
		let {
			filterType
		} = this.props

		if (this.props.hasErrored) {
			return <div className="container--center">Sorry! There was an error loading the items</div>
		}

		if (this.props.isLoading) {
			return <div className="container--center">Loading…</div>
		}

		return (
			<div className=".container">
				<ul className="grid">
					{
						this.props.items.resources &&
						this.props.items.resources
							.filter(function(item) {
								if (filterType === 'hiddenbits') {
									return item
								} else {
									return item.context.custom.bittype === filterType
								}
							})
							.sort(function(a, b) {
								const dateA = new Date(a.context.custom.datecreated), dateB = new Date(b.context.custom.datecreated)
								return dateB - dateA
							})
							.map((item) => ( 
						<li key = { item.public_id }>
							<Bit
								id = { item.public_id }
								homie = { item.context.custom.homie }
								dateofbirth = { item.context.custom.dateofbirth }
								title = { item.context.custom.title }
								datecreated = { item.context.custom.datecreated }
								description = { item.context.custom.description }
								projectlink = { item.context.custom.projectlink }
								toolsused = { item.context.custom.toolsused }
							>
							</Bit>
						</li>
					))}
				</ul>
				<MoreInfoCardContainer />
			</div>
		)
	}
}

export default BitsOverview
