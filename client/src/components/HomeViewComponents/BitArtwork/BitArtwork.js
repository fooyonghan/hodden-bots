import React from 'react'
import { Image, CloudinaryContext } from 'cloudinary-react'

import './BitArtwork.scss'

const BitArtwork = ({
	id
}) =>
	<div className="bitArtwork"
		id = { id }
	>
		<CloudinaryContext cloudName="fooyonghan">
			<Image
				publicId={id}
			/>
		</CloudinaryContext>
	</div>

export default BitArtwork