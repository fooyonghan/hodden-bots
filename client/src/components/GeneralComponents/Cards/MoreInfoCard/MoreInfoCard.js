import React, { Component } from 'react'
import moment from 'moment'

import './MoreInfoCard.scss'

class MoreInfoCard extends Component {
	constructor(props) {
		super(props)
		this.handleMouseDown = this.handleMouseDown.bind(this)
	}
	
	handleMouseDown(e) {
		this.props.onHideMoreInfoCard()
		e.stopPropagation()
	}

	render() {
		let {
			visibleMoreInfoCard,
			card
		} = this.props

		let cardClass = visibleMoreInfoCard ? 'show-card' : 'hide-card'

		if (Object.keys(this.props.card).length === 0 && this.props.card.constructor === Object) {
			cardClass = 'no-card'
		}

		return (
			<div className={`card-container ${cardClass}`}>
				<div className="close-extra-card" onMouseDown={this.handleMouseDown}>X</div>
				<div className="extra-info-container">
					<div className="title">{ card.title }</div>
					<div className="datecreated">{ moment(card.datecreated).format('MMMM YYYY') }</div>
				</div>
				<div className="extra-info-container">
					<div className="homie">{ card.homie }</div>
					<div className="dateofbirth">b. { moment(card.dateofbirth).format('YYYY') }</div>
				</div>
				<div className="description">{ card.description }</div>
				<div className="extra-info-container">
					<div className="toolsused">{ card.toolsused }</div>
					<div className="projectlink"><a href={ card.projectlink }>{ card.projectlink }</a></div>
				</div>
			</div>
		)
	}
}

export default MoreInfoCard