import React, { Component } from 'react'
import moment from 'moment'

import './BitCard.scss'

class BitCard extends Component {
	constructor(props) {
		super(props)
		this.onClickHandler = this.onClickHandler.bind(this)
	}

	onClickHandler() {
		const {
			onShowMoreInfoCard,
		} = this.props
		
		const selectedCard = {
			description: this.props.description,
			projectlink: this.props.projectlink,
			toolsused: this.props.toolsused,
			homie: this.props.homie,
			dateofbirth: this.props.dateofbirth,
			title: this.props.title,
			datecreated: this.props.datecreated
		}

		onShowMoreInfoCard(
			selectedCard
		)
	}

	render() {
		const cardClass =
			this.props.card.title === this.props.title && this.props.visibleMoreInfoCard !== false
				? 'hide-card'
				: 'show-card'

		return (
			<div
				className={`bit-card ${cardClass}`}
				onClick={this.onClickHandler}
			>
				<div className="title">{ this.props.title }</div>
				<div className="datecreated">{ moment(this.props.datecreated).format('MMMM YYYY') }</div>
				<div className="toolsused">{ this.props.toolsused }</div>
			</div>
		)
	}
}

export default BitCard