import React, { Component } from 'react'
import './Footer.scss'

class Footer extends Component {
	render () {
		return (
			<div className="footer">
				<small>&copy; Copyright 2019, Hidden Bits</small>
			</div>
		)
	}
}

export default Footer
