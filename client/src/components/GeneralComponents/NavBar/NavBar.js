import React, { Component } from 'react'
import './NavBar.scss'

import NavBarItem from './NavBarItem'
import Maya from '../Maya/Maya'
import CardContainer from '../../../containers/CardContainer';

class NavBar extends Component {
	constructor(props) {
		super(props)
		this.handleMouseDown = this.handleMouseDown.bind(this)
	}
	
	handleMouseDown(e) {
		this.props.onHideMobileNavigationMenu()
		e.stopPropagation()
	}

	render() {
		let {
			visibleMobileMenu,
			onHideMobileNavigationMenu
		} = this.props

		const navBarClass = visibleMobileMenu ? 'show-menu' : 'hide-menu'

		return (
			<div className="navbar">
				<div onMouseDown={this.handleMouseDown} className={`overlay-container ${navBarClass}`}></div>
				<div
					className={`sidebar-container ${navBarClass}`}>
					<aside className="row">
						<ul className="sidebar">
							<NavBarItem
								path="/"
								onHideMobileNavigationMenu={onHideMobileNavigationMenu}
								linkText="Home"
							/>

							<NavBarItem
								path="/about"
								onHideMobileNavigationMenu={onHideMobileNavigationMenu}
								linkText="About"
							/>

							<NavBarItem
								path="/contact"
								onHideMobileNavigationMenu={onHideMobileNavigationMenu}
								linkText="Contact"
							/>

							<Maya/>
							<CardContainer
								homie = 'T.H. Lie'
								dateofbirth = '1989-02-02T00:00:00.000Z'
								title = 'Maya'
								datecreated = '2018-02-23T00:00:00.000Z'
								description = 'Maya is somewhat of a mascot for Hidden Bits. She used to be the museum guide, but has been retired.'
								projectlink = 'http://hiddenbits.herokuapp.com'
								toolsused = 'GSAP, SVG, Affinity Designer'
							/>
						</ul>
					</aside>
				</div>
			</div>
		)
	}
}

export default NavBar