import React, { Component } from 'react'
import { TweenLite, TimelineLite } from 'gsap'
import './Eyecon.scss'


class Eyecon extends Component {
	constructor(props) {
		super(props)
		this.onClickHandler = this.onClickHandler.bind(this)
	}

	componentDidMount() {
		this.eyeconElements = {
			eyeball: this.eyeball
		}
		this.animateEyecon()
	}

	animateEyecon() {
		const eyeball = this.eyeconElements.eyeball
		this.randomMovement(eyeball)
	}

	randomMovement (element) {
		TweenLite.to(element, 0.4, {
			x: -30 + (32 * Math.random()),
			y: -10 + (20 * Math.random())
		})

		if (Math.random() >= 0.5) this.blink (element)

		var self = this
		setTimeout(function () {
			self.randomMovement(element)
		}, Math.random() * 8000)
	}

	blink (element) {
		let tl = new TimelineLite( )
		let closeEye = TweenLite.to(element, 0.2, { scaleY: 0, transformOrigin: "center center", delay:0.1 })
		let openEye = TweenLite.to(element, 0.2, { scaleY: 1, transformOrigin: "center center" })
	
		tl.add( closeEye )
		tl.add( openEye )
	}

	onClickHandler() {
		const {
			showSideBarMenu,
			onShowMobileNavigationMenu,
			onHideMobileNavigationMenu
		} = this.props
		
		showSideBarMenu ? onShowMobileNavigationMenu() : onHideMobileNavigationMenu()
	}

	render() {
		let {
			visibleMobileMenu
		} = this.props

		const eyeClass = visibleMobileMenu ? 'open' : 'close'

		return (
			<div className={`eyecontainer ${eyeClass} roll-in-blurred-right`} onClick={this.onClickHandler}>
				<svg
					width="100%"
					height="100%"
					viewBox="0 0 100 170"
					version="1.1"
					xmlns="http://www.w3.org/2000/svg"
					xmlnsXlink="http://www.w3.org/1999/xlink"
					style={{}}
				>
					<path
						className="diamond"
						d="M50,0l50,120l-50,50l-50,-50l50,-120Z"
						style={{fill:"#97B18C"}}
					/>
					<ellipse
						className="eyeball"
						cx="61.818"
						cy="116.967"
						rx="20.189"
						ry="20"
						style={{fill:"#ffffff"}}
						ref={(ref)=>this.eyeball=ref}
					/>
				</svg>
			</div>
		)
	}
}

export default Eyecon
