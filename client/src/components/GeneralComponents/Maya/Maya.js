import React from 'react'
import { TweenLite } from 'gsap'

import './Maya.scss'

class Maya extends React.Component {
	componentDidMount() {
		this.mayaElements = {
			eyeball: this.eyeball
		}
		this.animateMaya()
	}

	animateMaya() {
		const eyeball = this.mayaElements.eyeball
		this.randomMovement(eyeball)
	}

	randomMovement (element) {
		TweenLite.to(element, 0.4, {
			x: -30 + (60 * Math.random()),
			y: -10 + (20 * Math.random())
		})

		var self = this
		setTimeout(function () {
			self.randomMovement(element)
		}, Math.random() * 4000)
	}

	render() {
		return (
			<div
				className="maya-frame"
				// onClick={ (e) => this.animateMaya(e) }
			>
				<div className="maya">
					<svg
						width={"100%"}
						height={"100%"}
						style={{}}
						viewBox={"0 0 100 170"}
						xmlns="http://www.w3.org/2000/svg"
						xmlnsXlink="http://www.w3.org/1999/xlink"
						className={`svg-icon ${"" || ""}`}
					>
						<path
							id="diamond"
							d="M50,0l50,120l-50,50l-50,-50l50,-120Z"
							style={{fill:"url(#backgroundgradient)"}}
						/>
						<path
							id="eyelid"
							d="M0,120.005c0,0 5.591,-0.151 18.023,-6.814c8.111,-4.347 20.128,-16.224 31.977,-16.224c11.849,0 23.896,11.877 32.008,16.224c12.432,6.663 17.992,6.814 17.992,6.814c0,0 -5.355,1.149 -17.992,7.823c-8.064,4.258 -20.357,15.164 -32.008,15.164c-11.553,0 -23.946,-10.955 -31.977,-15.164c-12.762,-6.689 -18.023,-7.823 -18.023,-7.823Z"
						/>
						<path
							id="eyewhite"
							d="M0,120c0,0 5.591,-0.113 18.023,-5.091c8.111,-3.248 20.19,-13.209 32.039,-13.209c11.849,0 23.834,9.961 31.946,13.209c12.432,4.978 17.992,5.091 17.992,5.091c0,0 -5.355,0.858 -17.992,5.844c-8.064,3.182 -20.118,12.572 -31.77,12.572c-11.552,0 -24.184,-9.427 -32.215,-12.572c-12.762,-4.997 -18.023,-5.844 -18.023,-5.844Z"
							style={{fill:"#fff"}}
						/>
						<clipPath id="_clip1">
							<path
								d="M0,120c0,0 5.591,-0.113 18.023,-5.091c8.111,-3.248 20.19,-13.209 32.039,-13.209c11.849,0 23.834,9.961 31.946,13.209c12.432,4.978 17.992,5.091 17.992,5.091c0,0 -5.355,0.858 -17.992,5.844c-8.064,3.182 -20.118,12.572 -31.77,12.572c-11.552,0 -24.184,-9.427 -32.215,-12.572c-12.762,-4.997 -18.023,-5.844 -18.023,-5.844Z"
							/>
						</clipPath>
						<g
							clipPath="url(#_clip1)"
						>
							<ellipse
								id="eyeball"
								cx="50"
								cy="119.958"
								rx="20.189"
								ry="20"
								style={{fill:"url(#eyeballgradient)",stroke:"darkgrey",strokeWidth:5}}
								className="eyeball"
								ref={(ref)=>this.eyeball=ref}
							/>
						</g>

						<defs>
							<radialGradient
								id="backgroundgradient"
								cx="50%"
								cy="50%"
								r="73%"
								fx="50%"
								fy="80%"
								gradientUnits="objectBoundingBox"
							>
								<stop stopColor="#c0beba" offset="10%"/>
								<stop stopColor="#484745" offset="100%"/>
							</radialGradient>
						</defs>

						<defs>
							<radialGradient
								id="eyeballgradient"
								cx="50%"
								cy="50%"
								r="50%"
								fx="50%"
								fy="50%"
							>
								<stop offset="0%" style={{stopColor:"rgb(0,0,0)", stopOpacity:0.4}} />
								<stop offset="100%" style={{stopColor:"rgb(0,0,0)", stopOpacity:1}} />
							</radialGradient>
						</defs>

					</svg>
				</div>
			</div>
		)
	}
}

export default Maya