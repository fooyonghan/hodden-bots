import React, { Component } from 'react'

import EyeContainer from '../../../containers/EyeContainer'
import NavBarContainer from '../../../containers/NavBarContainer'

import './Header.scss'

class Header extends Component {
	constructor(props) {
		super(props)
		this.onClickHandler = this.onClickHandler.bind(this)
		this.state = {
      headerText: 'hidden bits'
    }
	}

	onClickHandler() {
		const {
			onShowTinyBits,
			onShowMassiveBits,
			onShowHiddenBits
		} = this.props

		if (this.props.filterType==='hiddenbits') {
			onShowTinyBits()
			this.setState({ headerText: 'tiny bits' })
		} else if (this.props.filterType==='tinybits') {
			onShowMassiveBits()
			this.setState({ headerText: 'massive bits' })
		} else {
			onShowHiddenBits()
			this.setState({ headerText: 'hidden bits' })
		}
	}

	render () {
		return (
			<div className="header">
				<div className="hidden-bits flicker-in-1">
					<h1 onClick={this.onClickHandler}>
						{this.state.headerText}
					</h1>
				</div>
				<NavBarContainer />
				<EyeContainer showSideBarMenu />
			</div>
		)
	}
}

export default Header
