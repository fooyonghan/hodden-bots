import React from 'react'
import './EyeShape.scss'

const EyeShape = () =>
	<div className="eyeshape">
		<svg
			width="100%"
			height="100%"
			viewBox="0 0 100 170"
			version="1.1"
			xmlns="http://www.w3.org/2000/svg"
			xmlnsXlink="http://www.w3.org/1999/xlink"
			style={{}}
		>
			<path
				className="diamond"
				d="M50,0l50,120l-50,50l-50,-50l50,-120Z"
				style={{fill:"#97B18C"}}
			/>
			<ellipse
				className="eyeball"
				cx="61.818"
				cy="116.967"
				rx="20.189"
				ry="20"
				style={{fill:"#ffffff"}}
			/>
		</svg>
	</div>

export default EyeShape