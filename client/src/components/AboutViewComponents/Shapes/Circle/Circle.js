import React, { Component } from 'react'
import './Circle.scss'

class Circle extends Component {
	constructor(props){
		super(props)
		
    this.state = {
			w: Math.floor(Math.random() * 41) + 20,
			h: Math.floor(Math.random() * 31) + 20
		}
	}

	render () {
		const shapeStyle = {
			width: this.state.w + "px",
			height: this.state.w + "px"
		}

		return (
			<div className="circle" style={shapeStyle}>
			</div>
		)
	}
}

export default Circle