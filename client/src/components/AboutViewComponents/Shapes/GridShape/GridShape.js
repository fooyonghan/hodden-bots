import React from 'react'
import './GridShape.scss'
import EyeShape from '../EyeShape/EyeShape'
import Monstera from '../Monstera/Monstera'
import Keith from '../Keith/Keith'
import Zen from '../Zen/Zen'
import Heart from '../Heart/Heart'

const GridShape = (shape) =>
	<div className="grid-shape">
		<div className="square"></div>
		<div className="grid-overlay"></div>
		{(() => {
        switch(shape.shape) {
					case 'eye':
						return <EyeShape></EyeShape>
					case 'keith':
						return <Keith></Keith>
          case 'zen':
            return <Zen></Zen>
          case 'monstera':
            return <Monstera></Monstera>
					case 'heart':
						return <Heart></Heart>
          default:
            return null;
        }
      })()}
		}
	</div>

export default GridShape