import React from 'react'
import './Keith.scss'

const Keith = () =>
	<div className="keith">
		<svg
			width={"100%"}
			height={"100%"}
			viewBox="0 0 256 256"
			style={{}}
			xmlns="http://www.w3.org/2000/svg"
			xmlnsXlink="http://www.w3.org/1999/xlink"
			className={`svg-icon ${"" || ""}`}
		>
			<path
				id="puppet"
				d="M85.926,97.428l-21.726,-45.61c-5.031,3.425 -42.352,-4.651 1.806,-19.483l26.545,43.495c-2.435,1.633 20.71,-10.428 20.71,-10.428c-26.959,4.18 -30.726,-24.334 0,-33.067c15.547,-4.419 42.771,8.524 37.887,28.711l27.051,-4.956l23.113,53.767c0,0 37.301,-7.654 -1.43,20.91l-28.399,-42.127c-1.554,15.011 -1.27,40.312 -42.514,80.225l23.84,52.541c0,0 -61.423,33.868 -16.04,-8.718l-32.711,-34.858l9.203,-31.496l-43.11,13.796l-27.121,28.33c0,0 -55.349,-36.777 2.966,-19.595l11.465,-27.097l50.508,-22.832c5.032,-7.564 8.338,-17.411 9.238,-30.439l-31.281,8.931Z"
				style={{fill:"#CBD8C5", stroke:"#214912", strokeWidth:"3px"}}
			/>
			<path
				d="M47.52,25.229c0,0 -15.724,2.456 -14.589,19.285"
				style={{fill:"none", stroke:"#214912", strokeWidth:"3px"}}
			/>
			<path
				d="M40.196,21.827c0,0 -15.724,2.456 -14.589,19.284"
				style={{fill:"none", stroke:"#214912", strokeWidth:"3px"}}
			/>
			<path
				d="M217.474,98.976c12.088,5.847 13.653,9.774 8.924,20.916"
				style={{fill:"none", stroke:"#214912", strokeWidth:"3px"}}
			/>
			<path
				d="M225.315,95.673c12.088,5.847 13.653,9.774 8.924,20.916"
				style={{fill:"none", stroke:"#214912", strokeWidth:"3px"}}
			/>
			<path
				d="M19.286,182.245c0,0 13.996,17.216 20.91,16.016"
				style={{fill:"none", stroke:"#214912", strokeWidth:"3px"}}
			/>
			<path
				d="M19.286,190.283c0,0 13.996,17.216 20.91,16.016"
				style={{fill:"none", stroke:"#214912", strokeWidth:"3px"}}
			/>
		</svg>
	</div>

export default Keith

