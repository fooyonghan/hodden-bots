import React from 'react'
import './Heart.scss'

const Heart = () =>
	<div className="heart">
		<svg
			width={"100%"}
			height={"100%"}
			viewBox="0 0 256 256"
			style={{}}
			xmlns="http://www.w3.org/2000/svg"
			xmlnsXlink="http://www.w3.org/1999/xlink"
			className={`svg-icon ${"" || ""}`}
		>
			<path
				id="topright"
				d="M256,128l0,-88l-40,-40l-48,0l-40,40l0,88"
				style={{fill:"#214912"}}
			/>
			<path
				id="bottomright"
				d="M128,128l0,128l128,-128"
				style={{fill:"#97B18C"}}
			/>
			<path
				id="left"
				d="M128,40l-40,-40l-48,0l-40,40l0,88l128,128l0,-216Z"
				style={{fill:"#CBD8C5"}}
			/>
		</svg>
	</div>

export default Heart