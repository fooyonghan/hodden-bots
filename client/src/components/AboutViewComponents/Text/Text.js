import React, { Component } from 'react'
import './Text.scss'

class Text extends Component {
	htmlDecode(input){
		var e = document.createElement('div')
		e.innerHTML = input
		return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue
	}
	
	render () {
		return (
			<div className="sentence">
				<div className="reveal-holder">
					<div className="reveal-block right" data-aos="reveal-right" />
					<div dangerouslySetInnerHTML={{ __html: this.htmlDecode(this.props.text) }} />
				</div>
			</div>
		)
	}
}

export default Text