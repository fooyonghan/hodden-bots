import p5 from 'p5'

export default function sketch(p){
	const flock = []

	p.setup = () => {
		p.createCanvas(window.innerWidth, window.innerHeight)

		for (let i=0; i<40; i++) {
			flock.push(new Boid())
		}
	}

	p.draw = () => {
		p.background('whitesmoke')
		
		for (let boid of flock) {
			boid.checkBorders()
			boid.redirect(boid.findFellowBoids(flock))
			boid.update()
			boid.show()
		}
	}

	class Boid {
		constructor() {
			this.size = 11
			this.position = p.createVector(p.random(window.innerWidth), p.random(window.innerHeight))
			let vector = p5.Vector.random2D()
			this.velocity = p.createVector(vector.x, vector.y)
			this.velocity.setMag(p.random(2, 4))
			this.acceleration = p.createVector()
			this.topSpeed = 8
			this.maxForce = 1.2
			this.viewingRadius = 50
		}

		checkBorders () {
			if (this.position.x > window.innerWidth) {
				this.position.x = 0
			} else if (this.position.x < 0) {
				this.position.x = window.innerWidth
			}

			if (this.position.y > window.innerHeight) {
				this.position.y = 0
			} else if (this.position.y < 0) {
				this.position.y = window.innerHeight
			}
		}

		findFellowBoids (boids) {
			let fellowBoids = []
			for (let otherBoid of boids) {
				let distance = p.dist(
					this.position.x,
					this.position.y,
					otherBoid.position.x,
					otherBoid.position.y
				)

				if (distance < this.viewingRadius && otherBoid !== this) {
					fellowBoids.push(otherBoid)
				}
			}
			return fellowBoids
		}

		// Accelerate away from mean distance
		separation (boids) {
			let steerTo = p.createVector()
			let nearbyBoids = 0

			for (let otherBoid of boids) {
				let distance = p.dist(
					this.position.x,
					this.position.y,
					otherBoid.position.x,
					otherBoid.position.y
				)

				if (distance < this.viewingRadius && otherBoid !== this) {
					let diff = p5.Vector.sub(this.position, otherBoid.position)
					diff.div(distance * distance)
					steerTo.add(diff)
					nearbyBoids += 1
				}
			}

			if (nearbyBoids > 0) {
				steerTo.div(nearbyBoids)
				steerTo.setMag(this.topSpeed)
				steerTo.sub(this.velocity)
				steerTo.limit(this.maxForce)
			}

			return steerTo
		}

		// Accelerate towards mean velocity
		align (boids) {
			let steerTo = p.createVector()
			let nearbyBoids = 0

			for (let otherBoid of boids) {
				let distance = p.dist(
					this.position.x,
					this.position.y,
					otherBoid.position.x,
					otherBoid.position.y
				)

				if (distance < this.viewingRadius && otherBoid !== this) {
					steerTo.add(otherBoid.velocity)
					nearbyBoids += 1
				}
			}

			if (nearbyBoids > 0) {
				steerTo.div(nearbyBoids)
				
				steerTo.setMag(this.topSpeed)
				steerTo.sub(this.velocity)
				steerTo.limit(this.maxForce)
			}

			return steerTo
		}

		// Accelerate towards mean position
		cohesion (boids) {
			let steerTo = p.createVector()
			let nearbyBoids = 0
			
			for (let otherBoid of boids) {
				let distance = p.dist(
					this.position.x,
					this.position.y,
					otherBoid.position.x,
					otherBoid.position.y
				)
				
				if (distance < this.viewingRadius && otherBoid !== this) {
					steerTo.add(otherBoid.position)
					nearbyBoids += 1
				}
			}

			if (nearbyBoids > 0) {
				// Set mean position
				steerTo.div(nearbyBoids)
				steerTo.sub(this.position)

				steerTo.setMag(this.topSpeed)
				steerTo.sub(this.velocity)
				steerTo.limit(this.maxForce)
			}

			return steerTo
		}

		redirect (boids) {
			let separation = this.separation(boids)
			let align = this.align(boids)
			let cohesion = this.cohesion(boids)
			
			separation.mult(1.1)

			this.acceleration.add(separation)
			this.acceleration.add(align)
			this.acceleration.add(cohesion)
		}

		update () {
			this.position.add(this.velocity)
			this.velocity.add(this.acceleration)
			this.velocity.limit(this.topSpeed)
			this.acceleration.mult(0)
		}

		show () {
			p.strokeWeight(this.size)
			p.stroke(203, 216, 197)
			p.point(this.position.x, this.position.y)
		}
	}
}