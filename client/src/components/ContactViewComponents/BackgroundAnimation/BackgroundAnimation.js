import React, { Component } from 'react'
import p5 from 'p5'
import boidsSketch from './boidsSketch'
import './BackgroundAnimation.scss'

class BackgroundAnimation extends Component {
  componentDidMount() {
    this.canvas = new p5(boidsSketch, "p5-container")
  }

  shouldComponentUpdate() {
    return true
  }

  componentWillUnmount() {
    this.canvas.remove()
	}

	render () {
		return (
			<div
				className="background-animation"
				id="p5-container"
			/>
		)
	}
}

export default BackgroundAnimation
