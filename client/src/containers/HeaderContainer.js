import { connect } from 'react-redux'
import {
	showTinyBits,
	showMassiveBits,
	showHiddenBits
} from '../store/actions/FilterAction'

import Header from '../components/GeneralComponents/Header/Header'

const mapStateToProps = (state) => {
	return {
		filterType: state.FilterReducer.filterType
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		onShowTinyBits: () => dispatch(showTinyBits()),
		onShowMassiveBits: () => dispatch(showMassiveBits()),
		onShowHiddenBits: () => dispatch(showHiddenBits())
	}
}

const HeaderContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(Header)

export default HeaderContainer
