import { connect } from 'react-redux'
import {
	hideMobileNavigationMenu,
	showMobileNavigationMenu
} from '../store/actions/EyeAction'

import Eyecon from '../components/GeneralComponents/Eyecon/Eyecon'

const mapStateToProps = (state, ownProps) => {
	return {
		visibleMobileMenu: state.BurgerReducer.visibleMobileMenu,
		...ownProps
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		onHideMobileNavigationMenu: () => dispatch(hideMobileNavigationMenu()),
		onShowMobileNavigationMenu: () => dispatch(showMobileNavigationMenu())
	}
}

const EyeContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(Eyecon)

export default EyeContainer
