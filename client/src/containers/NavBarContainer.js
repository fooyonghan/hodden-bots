import { connect } from 'react-redux'
import {
	hideMobileNavigationMenu
} from '../store/actions/EyeAction'

import NavBar from '../components/GeneralComponents/NavBar/NavBar'

const mapStateToProps = (state) => {
	return {
		visibleMobileMenu: state.BurgerReducer.visibleMobileMenu
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		onHideMobileNavigationMenu: () => dispatch(hideMobileNavigationMenu())
	}
}

const NavBarContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(NavBar)

export default NavBarContainer
