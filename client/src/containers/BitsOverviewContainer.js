import { connect } from 'react-redux'
import { itemsFetchData } from '../store/actions/LoadBitsAction'

import BitsOverview from '../components/HomeViewComponents/BitsOverview/BitsOverview'

const mapStateToProps = (state) => {
	return {
		items: state.items,
		hasErrored: state.itemsHasErrored,
		isLoading: state.itemsIsLoading,
		filterType: state.FilterReducer.filterType
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		fetchData: (url) => dispatch(itemsFetchData(url))
	}
}

const BitsOverviewContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(BitsOverview)

export default BitsOverviewContainer
