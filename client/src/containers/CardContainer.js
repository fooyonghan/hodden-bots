import { connect } from 'react-redux'
import {
	showMoreInfoCard,
	hideMoreInfoCard,
} from '../store/actions/CardAction'

import BitCard from '../components/GeneralComponents/Cards/BitCard/BitCard'

const mapStateToProps = (state, ownProps) => {
	return {
		visibleMoreInfoCard: state.CardReducer.visibleMoreInfoCard,
		card: state.CardReducer.card,
		...ownProps
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		onShowMoreInfoCard: (card) => dispatch(showMoreInfoCard(card)),
		onHideMoreInfoCard: () => dispatch(hideMoreInfoCard())
	}
}

const CardContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(BitCard)

export default CardContainer
