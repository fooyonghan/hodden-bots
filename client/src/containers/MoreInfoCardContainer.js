import { connect } from 'react-redux'
import {
	hideMoreInfoCard
} from '../store/actions/CardAction'

import MoreInfoCard from '../components/GeneralComponents/Cards/MoreInfoCard/MoreInfoCard'

const mapStateToProps = (state) => {
	return {
		visibleMoreInfoCard: state.CardReducer.visibleMoreInfoCard,
		card: state.CardReducer.card
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		onHideMoreInfoCard: () => dispatch(hideMoreInfoCard())
	}
}

const MoreInfoCardContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(MoreInfoCard)

export default MoreInfoCardContainer
